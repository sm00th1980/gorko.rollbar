'use strict';

var isFunction = require('lodash/isFunction');
var isUndefined = require('lodash/isUndefined');
var compact = require('lodash/compact');
var isEmpty = require('lodash/isEmpty');
var extend = require('lodash/extend');
var ENV = require('./env');
var RollbarLibrary = require('../vendor/rollbar.umd.min.js');

var MAX_ERRORS_PER_MINUTE_TO_ROLLBAR = 3;
var MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR = 3;

var NOT_AUTHORIZED_USER_ID = 'not_authorized';
var GET_USER_ID_FN_THROW_ERROR = 'getUserIdFn throw ';

var LANGUAGE_CANNOT_BE_DETERMINED = 'language cannot be determined';
var GET_LANGUAGE_FN_THROW_ERROR = 'getLanguageFn throw ';

var VERSION_CANNOT_BE_DETERMINED = 'version cannot be determined';
var GET_VERSION_FN_THROW_ERROR = 'getVersionFn throw ';

var safeRun = function safeRun(fn, defaultValue, errorValue) {
  var value = void 0;
  try {
    value = fn() || defaultValue;
  } catch (e) {
    value = errorValue + e.toString();
  }

  return value;
};

var captureUncaught = function captureUncaught(env) {
  return ENV.STAGING === env;
};

var checkIgnore = function checkIgnore(env) {
  return function (isUncaught) {
    if (ENV.STAGING === env) return false;
    if (!isUncaught && ENV.PRODUCTION === env) return false;

    // ignore all errors by default
    return true;
  };
};

var normalizeEnvironment = function normalizeEnvironment(fn) {
  var env = void 0;
  try {
    env = fn();
  } catch (e) {
    env = ENV.LOCAL;
  }

  if (env) {
    if (ENV.PRODUCTION.toLowerCase() === env.toLowerCase()) return ENV.PRODUCTION;
    if (ENV.STAGING.toLowerCase() === env.toLowerCase()) return ENV.STAGING;
  }
  return ENV.LOCAL;
};

var PAYLOADS = [];
var getPayloads = function getPayloads() {
  var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : PAYLOADS;
  return payload;
};
var appendToPayloads = function appendToPayloads(payload) {
  return PAYLOADS.push(payload);
};
var resetPayloads = function resetPayloads() {
  return PAYLOADS = [];
};

var transform = function transform(getLanguageFn) {
  return function (payload) {
    // eslint-disable-next-line no-param-reassign
    payload.actions = getPayloads();

    // eslint-disable-next-line no-param-reassign
    payload.person = extend(payload.person, {
      locale: safeRun(getLanguageFn, LANGUAGE_CANNOT_BE_DETERMINED, GET_LANGUAGE_FN_THROW_ERROR)
    });
  };
};

var produceRollbarConfig = function produceRollbarConfig(config) {
  var checkIgnoreMock = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : checkIgnore;
  var transformMock = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : transform;
  return {
    accessToken: config.token,
    // record uncaught exceptions from window.onerror
    captureUncaught: captureUncaught(config.environment),
    // record unhandled Promise rejections via the window event unhandledrejection
    captureUnhandledRejections: true,
    itemsPerMinute: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
    maxItems: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
    payload: {
      environment: config.environment,
      client: {
        javascript: {
          source_map_enabled: false,
          code_version: safeRun(config.getVersionFn, VERSION_CANNOT_BE_DETERMINED, GET_VERSION_FN_THROW_ERROR),
          // Optionally have Rollbar guess which frames the error was thrown from
          // when the browser does not provide line and column numbers.
          guess_uncaught_frames: false
        }
      }
    },
    checkIgnore: checkIgnoreMock(config.environment),
    transform: transformMock(config.getLanguageFn)
  };
};

var CONFIG = {};
var setupRollbar = function setupRollbar() {
  var RollbarLibraryMock = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : RollbarLibrary;
  var configMock = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : CONFIG;
  var rollbarConfigMock = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : produceRollbarConfig;

  var Rollbar = new RollbarLibraryMock(rollbarConfigMock(configMock));

  var id = safeRun(configMock.getUserIdFn, NOT_AUTHORIZED_USER_ID, GET_USER_ID_FN_THROW_ERROR);

  Rollbar.configure({ payload: { person: { id: id } } });

  return Rollbar;
};

var config = function config() {
  var configuration = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
  var setupRollbarMock = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : setupRollbar;

  if (configuration) {
    var token = configuration.token,
        getEnvironmentFn = configuration.getEnvironmentFn,
        getUserIdFn = configuration.getUserIdFn,
        getLanguageFn = configuration.getLanguageFn,
        getVersionFn = configuration.getVersionFn;


    if (!isFunction(getEnvironmentFn)) throw new Error('getEnvironmentFn not a function');
    if (!isFunction(getUserIdFn)) throw new Error('getUserIdFn not a function');
    if (!isFunction(getLanguageFn)) throw new Error('getLanguageFn not a function');
    if (!isFunction(getVersionFn)) throw new Error('getVersionFn not a function');

    var tokenMessage = isUndefined(token) ? 'token' : null;
    var errorMessage = compact([tokenMessage]);
    if (!isEmpty(errorMessage)) throw new Error('Not enough params: ' + errorMessage.join(','));

    // all is good => save config and setup rollbar
    CONFIG.token = token;
    CONFIG.environment = normalizeEnvironment(getEnvironmentFn);
    CONFIG.getUserIdFn = getUserIdFn;
    CONFIG.getLanguageFn = getLanguageFn;
    CONFIG.getVersionFn = getVersionFn;

    setupRollbarMock();
  }

  return CONFIG;
};

var sendError = function sendError(message) {
  var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var rollbar = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : setupRollbar();
  return new Promise(function (resolveFn) {
    rollbar.error(message, {}, data, resolveFn);
  });
};

var sendInfo = function sendInfo(message) {
  var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var rollbar = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : setupRollbar();
  return new Promise(function (resolveFn) {
    rollbar.info(message, {}, data, resolveFn);
  });
};

var sendWarning = function sendWarning(message) {
  var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var rollbar = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : setupRollbar();
  return new Promise(function (resolveFn) {
    rollbar.warning(message, {}, data, resolveFn);
  });
};

module.exports = {
  config: config,
  captureUncaught: captureUncaught,
  checkIgnore: checkIgnore,
  sendError: sendError,
  sendInfo: sendInfo,
  sendWarning: sendWarning,
  setupRollbar: setupRollbar,
  produceRollbarConfig: produceRollbarConfig,
  MAX_ERRORS_PER_MINUTE_TO_ROLLBAR: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
  MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
  NOT_AUTHORIZED_USER_ID: NOT_AUTHORIZED_USER_ID,
  LANGUAGE_CANNOT_BE_DETERMINED: LANGUAGE_CANNOT_BE_DETERMINED,
  GET_USER_ID_FN_THROW_ERROR: GET_USER_ID_FN_THROW_ERROR,
  GET_LANGUAGE_FN_THROW_ERROR: GET_LANGUAGE_FN_THROW_ERROR,
  VERSION_CANNOT_BE_DETERMINED: VERSION_CANNOT_BE_DETERMINED,
  GET_VERSION_FN_THROW_ERROR: GET_VERSION_FN_THROW_ERROR,
  normalizeEnvironment: normalizeEnvironment,
  ENV: ENV,
  getPayloads: getPayloads,
  appendToPayloads: appendToPayloads,
  resetPayloads: resetPayloads
};