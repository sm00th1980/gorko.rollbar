'use strict';

module.exports = {
  PRODUCTION: 'production', // deployed on real production
  STAGING: 'staging', // deployed on staging
  LOCAL: 'local' // development/test local
};