# README

Init:
```
import RollbarService from 'gorko.rollbar';

// only one time
RollbarService.config({
	token: 'ROLLBAR_TOKEN', // required
	getVersionFn: () => 'VERSION_OF_CODE', // required
	getLanguageFn: () => 'CURRENT_LANGUAGE', // required
	getUserIdFn: () => current_user_id, // required
	getEnvironmentFn: () => RollbarService.ENV.PRODUCTION, // PRODUCTION, STAGING or LOCAL(by default)
});
```

Send error:
```
import RollbarService from 'gorko.rollbar';

// if you have error object
RollbarService.sendError(message, errorObject);

// without error object
RollbarService.sendError(message);
```