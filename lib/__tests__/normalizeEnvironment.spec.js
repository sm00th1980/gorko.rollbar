import { normalizeEnvironment, ENV } from '../rollbar';

describe('normalizeEnvironment', () => {
  it('should be staging for staging', () => {
    const getEnvironment = () => ENV.STAGING;

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.STAGING);
  });

  it('should be production for production', () => {
    const getEnvironment = () => ENV.PRODUCTION;

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.PRODUCTION);
  });

  it('should be local for local', () => {
    const getEnvironment = () => ENV.LOCAL;

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.LOCAL);
  });

  it('should be local for null', () => {
    const getEnvironment = () => null;

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.LOCAL);
  });

  it('should be local for undefined', () => {
    const getEnvironment = () => undefined;

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.LOCAL);
  });

  it('should be local for throw Error', () => {
    const getEnvironment = () => {
      throw new Error('oops');
    };

    expect(normalizeEnvironment(getEnvironment)).toBe(ENV.LOCAL);
  });
});
