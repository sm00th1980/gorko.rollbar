import {
  setupRollbar,
  NOT_AUTHORIZED_USER_ID,
  GET_USER_ID_FN_THROW_ERROR,
} from '../rollbar';

describe('setupRollbar', () => {
  it('should prepare rollbar lib with current user id', () => {
    const configureMock = jest.fn();
    const rollbarLibraryMock = jest.fn(() => ({ configure: configureMock }));
    const rollbarConfigMock = jest.fn(() => 'rollbarConfig');

    const configMock = {
      getUserIdFn: () => 123,
    };

    setupRollbar(rollbarLibraryMock, configMock, rollbarConfigMock);

    expect(rollbarLibraryMock).toHaveBeenCalledWith('rollbarConfig');
    expect(rollbarConfigMock).toHaveBeenCalledWith(configMock);
    expect(configureMock).toHaveBeenCalledWith({
      payload: {
        person: {
          id: 123,
        },
      },
    });
  });

  it('should prepare rollbar lib without user id and language', () => {
    const configureMock = jest.fn();
    const rollbarLibraryMock = jest.fn(() => ({ configure: configureMock }));
    const rollbarConfigMock = jest.fn(() => 'rollbarConfig');

    const configMock = {
      getUserIdFn: () => undefined,
    };

    setupRollbar(rollbarLibraryMock, configMock, rollbarConfigMock);

    expect(rollbarLibraryMock).toHaveBeenCalledWith('rollbarConfig');
    expect(rollbarConfigMock).toHaveBeenCalledWith(configMock);
    expect(configureMock).toHaveBeenCalledWith({
      payload: {
        person: {
          id: NOT_AUTHORIZED_USER_ID,
        },
      },
    });
  });

  it('should prepare rollbar lib if getUserIdFn throw error', () => {
    const configureMock = jest.fn();
    const rollbarLibraryMock = jest.fn(() => ({ configure: configureMock }));
    const rollbarConfigMock = jest.fn(() => 'rollbarConfig');

    const configMock = {
      getUserIdFn: () => {
        throw new Error('oops');
      },
    };

    setupRollbar(rollbarLibraryMock, configMock, rollbarConfigMock);

    expect(rollbarLibraryMock).toHaveBeenCalledWith('rollbarConfig');
    expect(rollbarConfigMock).toHaveBeenCalledWith(configMock);
    expect(configureMock).toHaveBeenCalledWith({
      payload: {
        person: {
          id: `${GET_USER_ID_FN_THROW_ERROR}Error: oops`,
        },
      },
    });
  });
});
