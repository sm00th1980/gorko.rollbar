import noop from 'lodash/noop';
import { normalizeEnvironment } from '../rollbar';

const RollbarService = require('../rollbar');
const RollbarService1 = require('../rollbar');
const RollbarService2 = require('../rollbar');

describe('captureUncaught', () => {
  it('should configure rollbar service as singleton', () => {
    expect(RollbarService1.config()).toEqual({});

    const configuration = {
      token: 'token',
      getEnvironmentFn: noop,
      getVersionFn: noop,
      getLanguageFn: noop,
      getUserIdFn: noop,
    };

    const setupRollbarMock = jest.fn();

    RollbarService1.config(configuration, setupRollbarMock);

    expect(RollbarService1.config()).toEqual({
      token: 'token',
      environment: normalizeEnvironment(noop),
      getVersionFn: noop,
      getLanguageFn: noop,
      getUserIdFn: noop,
    });

    expect(RollbarService2.config()).toEqual({
      token: 'token',
      environment: normalizeEnvironment(noop),
      getVersionFn: noop,
      getLanguageFn: noop,
      getUserIdFn: noop,
    });

    expect(setupRollbarMock).toHaveBeenCalled();
  });

  it('should throw error if not enough token configuration param', () => {
    const configuration = {
      getEnvironmentFn: noop,
      getVersionFn: noop,
      getLanguageFn: noop,
      getUserIdFn: noop,
    };

    const setupRollbarMock = jest.fn();
    expect(() => RollbarService.config(configuration, setupRollbarMock)).toThrowError(/Not enough params: token/);
    expect(setupRollbarMock).not.toHaveBeenCalled();
  });

  it('should throw error if getEnvironmentFn not a function', () => {
    const configuration = {
      getEnvironmentFn: 123,
      getVersionFn: noop,
      getUserIdFn: noop,
      getLanguageFn: noop,
    };

    const setupRollbarMock = jest.fn();
    expect(() => RollbarService.config(configuration)).toThrowError(/getEnvironmentFn not a function/);
    expect(setupRollbarMock).not.toHaveBeenCalled();
  });

  it('should throw error if getVersionFn not a function', () => {
    const configuration = {
      getEnvironmentFn: noop,
      getVersionFn: 123,
      getUserIdFn: noop,
      getLanguageFn: noop,
    };

    const setupRollbarMock = jest.fn();
    expect(() => RollbarService.config(configuration)).toThrowError(/getVersionFn not a function/);
    expect(setupRollbarMock).not.toHaveBeenCalled();
  });

  it('should throw error if getUserIdFn not a function', () => {
    const configuration = {
      getEnvironmentFn: noop,
      getVersionFn: noop,
      getUserIdFn: 123,
      getLanguageFn: noop,
    };

    const setupRollbarMock = jest.fn();
    expect(() => RollbarService.config(configuration)).toThrowError(/getUserIdFn not a function/);
    expect(setupRollbarMock).not.toHaveBeenCalled();
  });

  it('should throw error if getLanguageFn not a function', () => {
    const configuration = {
      getEnvironmentFn: noop,
      getVersionFn: noop,
      getUserIdFn: noop,
      etLanguageFn: 123,
    };

    const setupRollbarMock = jest.fn();
    expect(() => RollbarService.config(configuration)).toThrowError(/getLanguageFn not a function/);
    expect(setupRollbarMock).not.toHaveBeenCalled();
  });
});
