import { getPayloads, appendToPayloads, resetPayloads } from '../rollbar';

describe('payloads', () => {
  beforeEach(() => {
    resetPayloads();
  });

  it('should get initial payload as empty array', () => {
    expect(getPayloads()).toEqual([]);
  });

  it('should append one payload', () => {
    const payload = { action: 'action' };
    appendToPayloads(payload);
    expect(getPayloads()).toEqual([payload]);
  });

  it('should append two payloads', () => {
    const payload1 = { action1: 'action1' };
    const payload2 = { action2: 'action2' };

    appendToPayloads(payload1);
    appendToPayloads(payload2);
    expect(getPayloads()).toEqual([payload1, payload2]);
  });

  it('should reset payload at all', () => {
    const payload1 = { action1: 'action1' };
    const payload2 = { action2: 'action2' };

    appendToPayloads(payload1);
    appendToPayloads(payload2);
    expect(getPayloads()).toEqual([payload1, payload2]);

    resetPayloads();
    expect(getPayloads()).toEqual([]);
  });
});
