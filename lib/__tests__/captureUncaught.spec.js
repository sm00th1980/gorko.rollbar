import { captureUncaught } from '../rollbar';
import ENV from '../env';

describe('captureUncaught', () => {
  it('should capture all uncaught for staging', () => {
    expect(captureUncaught(ENV.STAGING)).toBe(true);
  });

  it('should not capture all uncaught for production', () => {
    expect(captureUncaught(ENV.PRODUCTION)).toBe(false);
  });

  it('should not capture all uncaught for local', () => {
    expect(captureUncaught(ENV.LOCAL)).toBe(false);
    expect(captureUncaught(undefined)).toBe(false);
  });
});
