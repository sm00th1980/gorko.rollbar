import noop from 'lodash/noop';
import {
  produceRollbarConfig,
  captureUncaught,
  MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
  MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
  VERSION_CANNOT_BE_DETERMINED,
  GET_VERSION_FN_THROW_ERROR,
} from '../rollbar';

describe('.produceRollbarConfig()', () => {
  it('should produce correct rollbar config', () => {
    const config = {
      token: 'token',
      environment: 'environment',
      getVersionFn: () => 'version',
      getLanguageFn: 'getLanguageFn',
    };

    const checkIgnoreMock = jest.fn(() => true);
    const transformMock = jest.fn(() => noop);

    const received = produceRollbarConfig(config, checkIgnoreMock, transformMock);

    const expected = {
      accessToken: config.token,
      captureUncaught: captureUncaught(config.environment),
      captureUnhandledRejections: true,
      itemsPerMinute: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
      maxItems: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
      payload: {
        environment: config.environment,
        client: {
          javascript: {
            source_map_enabled: false,
            code_version: 'version',
            guess_uncaught_frames: false,
          },
        },
      },
      checkIgnore: true,
      transform: noop,
    };

    expect(checkIgnoreMock).toHaveBeenCalledWith(config.environment);
    expect(transformMock).toHaveBeenCalledWith(config.getLanguageFn);

    expect(received).toEqual(expected);
  });

  it('should produce correct rollbar config when getVersionFn return undefined/null', () => {
    const config = {
      token: 'token',
      environment: 'environment',
      getVersionFn: () => undefined,
      getLanguageFn: 'getLanguageFn',
    };

    const checkIgnoreMock = jest.fn(() => true);
    const transformMock = jest.fn(() => noop);

    const received = produceRollbarConfig(config, checkIgnoreMock, transformMock);

    const expected = {
      accessToken: config.token,
      captureUncaught: captureUncaught(config.environment),
      captureUnhandledRejections: true,
      itemsPerMinute: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
      maxItems: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
      payload: {
        environment: config.environment,
        client: {
          javascript: {
            source_map_enabled: false,
            code_version: VERSION_CANNOT_BE_DETERMINED,
            guess_uncaught_frames: false,
          },
        },
      },
      checkIgnore: true,
      transform: noop,
    };

    expect(checkIgnoreMock).toHaveBeenCalledWith(config.environment);
    expect(transformMock).toHaveBeenCalledWith(config.getLanguageFn);

    expect(received).toEqual(expected);
  });

  it('should produce correct rollbar config when getVersionFn throw error', () => {
    const config = {
      token: 'token',
      environment: 'environment',
      getVersionFn: () => {
        throw new Error('oops');
      },
      getLanguageFn: 'getLanguageFn',
    };

    const checkIgnoreMock = jest.fn(() => true);
    const transformMock = jest.fn(() => noop);

    const received = produceRollbarConfig(config, checkIgnoreMock, transformMock);

    const expected = {
      accessToken: config.token,
      captureUncaught: captureUncaught(config.environment),
      captureUnhandledRejections: true,
      itemsPerMinute: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
      maxItems: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
      payload: {
        environment: config.environment,
        client: {
          javascript: {
            source_map_enabled: false,
            code_version: `${GET_VERSION_FN_THROW_ERROR}Error: oops`,
            guess_uncaught_frames: false,
          },
        },
      },
      checkIgnore: true,
      transform: noop,
    };

    expect(checkIgnoreMock).toHaveBeenCalledWith(config.environment);
    expect(transformMock).toHaveBeenCalledWith(config.getLanguageFn);

    expect(received).toEqual(expected);
  });
});
