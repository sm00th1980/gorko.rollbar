import { checkIgnore } from '../rollbar';
import ENV from '../env';

describe('.checkIgnore()', () => {
  describe('with uncaughtException', () => {
    let uncaughtException;
    beforeEach(() => {
      uncaughtException = true;
    });

    it('should not ignore uncaughtException for ENV.STAGING', () => {
      expect(checkIgnore(ENV.STAGING)(uncaughtException)).toBe(false);
    });

    it('should ignore uncaughtException for ENV.PRODUCTION', () => {
      expect(checkIgnore(ENV.PRODUCTION)(uncaughtException)).toBe(true);
    });

    it('should ignore uncaughtException for ENV.LOCAL', () => {
      expect(checkIgnore(ENV.LOCAL)(uncaughtException)).toBe(true);
      expect(checkIgnore(undefined)(uncaughtException)).toBe(true);
    });
  });

  describe('without uncaughtException', () => {
    let uncaughtException;
    beforeEach(() => {
      uncaughtException = false;
    });

    it('should not ignore non-uncaughtException for ENV.STAGING', () => {
      expect(checkIgnore(ENV.STAGING)(uncaughtException)).toBe(false);
    });

    it('should not ignore non-uncaughtException for ENV.PRODUCTION', () => {
      expect(checkIgnore(ENV.PRODUCTION)(uncaughtException)).toBe(false);
    });

    it('should ignore non-uncaughtException for ENV.LOCAL', () => {
      expect(checkIgnore(ENV.LOCAL)(uncaughtException)).toBe(true);
      expect(checkIgnore(undefined)(uncaughtException)).toBe(true);
    });
  });
});
