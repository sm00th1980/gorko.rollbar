import RollbarService from '../rollbar';

describe('RollbarService - .sendInfo without currentUser', () => {
  it('send info message to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    const data = { data: 'data' };
    let resolveFnMock;

    const infoMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { info: infoMock };

    await RollbarService.sendInfo(message, data, mockedRollbar);
    expect(infoMock).toHaveBeenCalledWith(message, {}, data, resolveFnMock);
  });

  it('send info message and empty error object to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    let resolveFnMock;

    const infoMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { info: infoMock };

    await RollbarService.sendInfo(message, undefined, mockedRollbar);
    expect(infoMock).toHaveBeenCalledWith(message, {}, {}, resolveFnMock);
  });
});
