import RollbarService from '../rollbar';

describe('RollbarService - .sendError without currentUser', () => {
  it('send error message to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    const data = { data: 'data' };
    let resolveFnMock;

    const errorMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { error: errorMock };

    await RollbarService.sendError(message, data, mockedRollbar);
    expect(errorMock).toHaveBeenCalledWith(message, {}, data, resolveFnMock);
  });

  it('send error message and empty error object to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    let resolveFnMock;

    const errorMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { error: errorMock };

    await RollbarService.sendError(message, undefined, mockedRollbar);
    expect(errorMock).toHaveBeenCalledWith(message, {}, {}, resolveFnMock);
  });
});
