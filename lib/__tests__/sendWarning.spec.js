import RollbarService from '../rollbar';

describe('RollbarService - .sendError without currentUser', () => {
  it('send warning message to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    const data = { data: 'data' };
    let resolveFnMock;

    const warningMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { warning: warningMock };

    await RollbarService.sendWarning(message, data, mockedRollbar);
    expect(warningMock).toHaveBeenCalledWith(message, {}, data, resolveFnMock);
  });

  it('send warning message and empty error object to rollbar and call callback after it', async () => {
    expect.assertions(1);

    const message = 'message';
    let resolveFnMock;

    const warningMock = jest.fn((mes, err, custom, resolveFn) => {
      resolveFnMock = resolveFn;
      resolveFn();
    });

    const mockedRollbar = { warning: warningMock };

    await RollbarService.sendWarning(message, undefined, mockedRollbar);
    expect(warningMock).toHaveBeenCalledWith(message, {}, {}, resolveFnMock);
  });
});
