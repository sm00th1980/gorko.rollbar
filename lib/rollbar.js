const isFunction = require('lodash/isFunction');
const isUndefined = require('lodash/isUndefined');
const compact = require('lodash/compact');
const isEmpty = require('lodash/isEmpty');
const extend = require('lodash/extend');
const ENV = require('./env');
const RollbarLibrary = require('../vendor/rollbar.umd.min.js');

const MAX_ERRORS_PER_MINUTE_TO_ROLLBAR = 3;
const MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR = 3;

const NOT_AUTHORIZED_USER_ID = 'not_authorized';
const GET_USER_ID_FN_THROW_ERROR = 'getUserIdFn throw ';

const LANGUAGE_CANNOT_BE_DETERMINED = 'language cannot be determined';
const GET_LANGUAGE_FN_THROW_ERROR = 'getLanguageFn throw ';

const VERSION_CANNOT_BE_DETERMINED = 'version cannot be determined';
const GET_VERSION_FN_THROW_ERROR = 'getVersionFn throw ';

const safeRun = (fn, defaultValue, errorValue) => {
  let value;
  try {
    value = fn() || defaultValue;
  } catch (e) {
    value = errorValue + e.toString();
  }

  return value;
};

const captureUncaught = env => ENV.STAGING === env;

const checkIgnore = env => (isUncaught) => {
  if (ENV.STAGING === env) return false;
  if (!isUncaught && ENV.PRODUCTION === env) return false;

  // ignore all errors by default
  return true;
};

const normalizeEnvironment = (fn) => {
  let env;
  try {
    env = fn();
  } catch (e) {
    env = ENV.LOCAL;
  }

  if (env) {
    if (ENV.PRODUCTION.toLowerCase() === env.toLowerCase()) return ENV.PRODUCTION;
    if (ENV.STAGING.toLowerCase() === env.toLowerCase()) return ENV.STAGING;
  }
  return ENV.LOCAL;
};

let PAYLOADS = [];
const getPayloads = (payload = PAYLOADS) => payload;
const appendToPayloads = payload => PAYLOADS.push(payload);
const resetPayloads = () => (PAYLOADS = []);

const transform = getLanguageFn => (payload) => {
  // eslint-disable-next-line no-param-reassign
  payload.actions = getPayloads();

  // eslint-disable-next-line no-param-reassign
  payload.person = extend(payload.person, {
    locale: safeRun(
      getLanguageFn, LANGUAGE_CANNOT_BE_DETERMINED, GET_LANGUAGE_FN_THROW_ERROR,
    ),
  });
};

const produceRollbarConfig = (
  config, checkIgnoreMock = checkIgnore, transformMock = transform,
) => ({
  accessToken: config.token,
  // record uncaught exceptions from window.onerror
  captureUncaught: captureUncaught(config.environment),
  // record unhandled Promise rejections via the window event unhandledrejection
  captureUnhandledRejections: true,
  itemsPerMinute: MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
  maxItems: MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
  payload: {
    environment: config.environment,
    client: {
      javascript: {
        source_map_enabled: false,
        code_version: safeRun(
          config.getVersionFn, VERSION_CANNOT_BE_DETERMINED, GET_VERSION_FN_THROW_ERROR,
        ),
        // Optionally have Rollbar guess which frames the error was thrown from
        // when the browser does not provide line and column numbers.
        guess_uncaught_frames: false,
      },
    },
  },
  checkIgnore: checkIgnoreMock(config.environment),
  transform: transformMock(config.getLanguageFn),
});

const CONFIG = {};
const setupRollbar = (
  RollbarLibraryMock = RollbarLibrary,
  configMock = CONFIG,
  rollbarConfigMock = produceRollbarConfig,
) => {
  const Rollbar = new RollbarLibraryMock(rollbarConfigMock(configMock));

  const id = safeRun(configMock.getUserIdFn, NOT_AUTHORIZED_USER_ID, GET_USER_ID_FN_THROW_ERROR);

  Rollbar.configure({ payload: { person: { id } } });

  return Rollbar;
};

const config = (configuration = undefined, setupRollbarMock = setupRollbar) => {
  if (configuration) {
    const { token, getEnvironmentFn, getUserIdFn, getLanguageFn, getVersionFn } = configuration;

    if (!isFunction(getEnvironmentFn)) throw new Error('getEnvironmentFn not a function');
    if (!isFunction(getUserIdFn)) throw new Error('getUserIdFn not a function');
    if (!isFunction(getLanguageFn)) throw new Error('getLanguageFn not a function');
    if (!isFunction(getVersionFn)) throw new Error('getVersionFn not a function');

    const tokenMessage = isUndefined(token) ? 'token' : null;
    const errorMessage = compact([
      tokenMessage,
    ]);
    if (!isEmpty(errorMessage)) throw new Error(`Not enough params: ${errorMessage.join(',')}`);

    // all is good => save config and setup rollbar
    CONFIG.token = token;
    CONFIG.environment = normalizeEnvironment(getEnvironmentFn);
    CONFIG.getUserIdFn = getUserIdFn;
    CONFIG.getLanguageFn = getLanguageFn;
    CONFIG.getVersionFn = getVersionFn;

    setupRollbarMock();
  }

  return CONFIG;
};

const sendError = (message, data = {}, rollbar = setupRollbar()) => new Promise((resolveFn) => {
  rollbar.error(message, {}, data, resolveFn);
});

const sendInfo = (message, data = {}, rollbar = setupRollbar()) => new Promise((resolveFn) => {
  rollbar.info(message, {}, data, resolveFn);
});

const sendWarning = (message, data = {}, rollbar = setupRollbar()) => new Promise((resolveFn) => {
  rollbar.warning(message, {}, data, resolveFn);
});

module.exports = {
  config,
  captureUncaught,
  checkIgnore,
  sendError,
  sendInfo,
  sendWarning,
  setupRollbar,
  produceRollbarConfig,
  MAX_ERRORS_PER_MINUTE_TO_ROLLBAR,
  MAX_ERRORS_PER_PAGE_LOAD_TO_ROLLBAR,
  NOT_AUTHORIZED_USER_ID,
  LANGUAGE_CANNOT_BE_DETERMINED,
  GET_USER_ID_FN_THROW_ERROR,
  GET_LANGUAGE_FN_THROW_ERROR,
  VERSION_CANNOT_BE_DETERMINED,
  GET_VERSION_FN_THROW_ERROR,
  normalizeEnvironment,
  ENV,
  getPayloads,
  appendToPayloads,
  resetPayloads,
};
